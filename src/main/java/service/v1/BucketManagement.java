package service.v1;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;


public class BucketManagement {

	public static final AtomicInteger id = new AtomicInteger( 0 );

	private List<Bucket> buckets;


	public void splitBucket( int bucketID ) {

		for( int i = 0; i < 100; ++i){

			System.out.println( "ID = " + buckets.get( i ).getID() );

			if( buckets.get( i ).getID() == bucketID ) {

				splitBucket( buckets.get( i ) );

				break;
			}
		}
	}

	private void splitBucket( Bucket b ) {

		System.err.println( "ID = " +  + b.getID() + ": Split implementation is incomplete." );
	}

	public void mergeBuckets( int bucketID1, int bucketID2 ) { }

	public void moveObject( int objectID, int fromBucketID, int toBucketID ) {

		Object ob = null;
		Bucket from = null;
		Bucket to = null;
		
		moveObject( ob, from, to );
	}

	private void moveObject( Object ob, Bucket from, Bucket to ) { }
	

	public void listBuckets() { }

	public void listObjects() { }

	public void setUpTestBuckets( List<Bucket> b ) {

		buckets = b;

		for( int i = 0; i < 100; ++i){

			Bucket bucket = new Bucket( id.incrementAndGet() );

			buckets.add( bucket );
		}
	}
}
